#!/bin/bash

# Set ulimit for benchmarking
ulimit -n 10000

kubectl delete -f config/workload.yml
npm install vm2@3.9.17 --save

file="templates/index.html"
line_number=20
new_line="          type: 'bar',"

# Escape special characters in the new_line variable
new_line_escaped=$(printf '%s\n' "$new_line" | sed -e 's/[]\/$*.^[]/\\&/g')

# Modify the line using awk
awk -v line_num=$line_number -v new_line="$new_line" 'NR == line_num { print new_line; next } { print }' "$file" > "$file.tmp"
mv "$file.tmp" "$file"

git commit -am "Pre-demo commit"
git push