#!/bin/bash
trap 'read -p "run: $BASH_COMMAND"' DEBUG

tanzu app workload apply -f config/workload.yml -y

# Fix CVE 
npm install vm2@3.9.18 --save
git commit -am "Demo fix cve"
git push