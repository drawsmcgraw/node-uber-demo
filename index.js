const express = require('express');
const fs = require('fs');
const path = require('path');
const { Pool } = require('pg');
const { from: copyFrom } = require('pg-copy-streams')
const serviceBindings = require('kube-service-bindings');
const morgan = require('morgan');

const app = express();
const port = process.env.PORT || 3000;

// Enable request logging
app.use(morgan('combined'));

// Static library set up
app.use('/apexcharts', express.static(path.join(__dirname, 'node_modules/apexcharts/dist')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));

let pgBinding;
try {
  // check if the app has been bound to a Postgres instance through
  // service bindings. If so use that connect info
  pgBinding = serviceBindings.getBinding('POSTGRESQL', 'pg');
} catch (err) { 
  console.log(err);
};

// Set up DB connection to postgres
const pool = new Pool({
  user: pgBinding.user || process.env.DB_USER,
  host: pgBinding.host || process.env.DB_HOST,
  database: pgBinding.database || process.env.DB_NAME,
  password: pgBinding.password || process.env.DB_PASSWORD,
  port: pgBinding.port || process.env.DB_PORT
});


const loadAndSeedData = async () => {
  try {
    const filePath = path.join(__dirname, 'data.csv');
    const client = await pool.connect();

    const createTableQuery = `
      CREATE TABLE IF NOT EXISTS uber (
        hour integer,
        count integer
      )
    `;
    await client.query(createTableQuery);

    const query = "SELECT COUNT(*) FROM uber";
    const result = await client.query(query);
    const uberCount = parseInt(result.rows[0].count);

    if (uberCount === 0) {
      const insertQuery = "COPY uber FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ',')";
      const stream = client.query(copyFrom(insertQuery));
      const fileStream = fs.createReadStream(filePath);

      fileStream.on('error', (error) => {
        console.error('Error reading file:', error);
      });

      fileStream.pipe(stream).on('finish', () => {
        console.log('Data has been loaded into the database');
        client.release();
      });
    } else {
      client.release();
    }

    return true;
  } catch (error) {
    console.error("Error seeding the database:", error.message);
    return false;
  }
};

// Call loadAndSeedData on app startup
(async () => {
  try {
    await pool.connect();
    await loadAndSeedData();
    console.log('Database seeded successfully');
  } catch (err) {
    console.error('Error seeding the database:', err);
    process.exit(1);
  }
})();

// Serve static files from the "public" directory
app.use(express.static('public'));

// Routes
app.get('/', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM uber');
    const data = result.rows.reduce((acc, row) => {
      acc[row.hour] = row.count;
      return acc;
    }, {});

    const filePath = path.join(__dirname, 'templates', 'index.html');
    const htmlTemplate = fs.readFileSync(filePath, 'utf8');

    const renderedTemplate = htmlTemplate.replace('{{data}}', JSON.stringify(data));

    res.send(renderedTemplate);
  } catch (error) {
    console.error('Error:', error);
    res.status(500).send('Internal Server Error');
  }
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
