# Node-Uber

Displays Uber data, stored in a postgres database, using NodeJS


# How to use

This app uses Kubernetes service bindings to connect to a Postgres database. Hence, you'll need to make a `ClassClaim` in Tanzu App Service.

Follow [the docs](https://docs.vmware.com/en/VMware-Tanzu-Application-Platform/1.5/tap/services-toolkit-how-to-guides-dynamic-provisioning-tanzu-postgresql.html) to set up the Postgres operator. This workflow configures your TAP installation to pull container images from your private repo, so it _WILL_ work in airgap deployments.

Create a ClassClaim with the following:

```
tanzu service class-claim create node-uber --class tanzu-psql --parameter storageGB=1 -n dev-tap
```

NOTE: The app is looking for a classClaim specifically named `node-uber`.

After creating the ClassClaim, apply the workload like any other:

```
tanzu app workload create -f workload.yml
```

If using with the accelerator, you will need to run `node csv.js` first to grab the example data file.